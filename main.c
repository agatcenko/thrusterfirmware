#include "stm8s.h"
#include "stm8s_it.h"
#include "thruster.h"
#include "uart_utils.h"

void initClocks()
{
  CLK_DeInit();
  
  // Work on 16MHz, external
  u8 cnt = 0;
  CLK_HSECmd(ENABLE);
  CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);
  CLK_ClockSwitchConfig(CLK_SWITCHMODE_AUTO, CLK_SOURCE_HSE, DISABLE, CLK_CURRENTCLOCKSTATE_DISABLE); 
  while (CLK_GetSYSCLKSource() != CLK_SOURCE_HSE) {
    if (--cnt == 0) {
      return;
    }
  }
}

void initPWM()
{
  TIM2_DeInit();
  TIM2_TimeBaseInit(TIM2_PRESCALER_8, 80); // 20 kHz
  TIM2_OC2Init(TIM2_OCMODE_PWM2, TIM2_OUTPUTSTATE_ENABLE, 0, TIM2_OCPOLARITY_HIGH);
  TIM2_Cmd(ENABLE);
}

int main( void )
{
  initClocks();
  initThruster();
  initPWM();
  initAdc();
  readEepromValues();
  initConnection();
    
  enableInterrupts();
  
  while (1) {
    
  }
  
  return 0;
}