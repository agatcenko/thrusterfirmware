# README #

Firmware for MUR thruster control unit. Use STM8S003 chip.

Before update firmware choose thruster address (THRUSTER_ADDRESS in /My_Lib/thruster.c file).
## Set address with one of these values ##
* 0x0A for address 10
* 0x14 for address 20
* 0x1E for address 30
* 0x28 for address 40