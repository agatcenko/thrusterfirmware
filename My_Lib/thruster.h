#ifndef THRUSTER_H
#define THRUSTER_H

#include "stm8s.h"
#include "stdlib.h"

#define PWM_FREQ 20000 // PWM frequency for thruster, 20 kHz

// Break pin, set 0 -> stop thruster
#define BREAK_PIN GPIO_PIN_5
#define BREAK_PORT GPIOC
// Thruster pin
#define THRUSTER_DIR_PORT GPIOC
#define THRUSTER_DIR_PIN GPIO_PIN_7

// Pins for calculate rpm (rotate per minute)
#define FG1_PORT GPIOC
#define FG1_PIN GPIO_PIN_6
#define FG2_PORT GPIOC
#define FG2_PIN GPIO_PIN_3

// Struct with thruster info
typedef struct ThrusterStatus
{
  uint16_t current; //current raw data
  uint16_t rpm; //rpm is divided by 6(Revolutions per 10s)
  int8_t power; // thruster power
} ThrusterStatus;

void setThrusterSpeed(int8_t speed); // Set new thruster power

void initAdc(); // Initialize adc1 and timer1
void initThruster(); // Initialize pins and timer4
void readEepromValues(); // Read thruster address from EEPROM storage

void readCurrent(); // Calculate current from raw adc data
void calculateRpm(); // Calculate RPM

void clearMsgTimer(); // Clear timer used for measure time without new thruster power command
void checkConnection(); // Power off thruster if last control msg was 3 sec ago 

uint8_t getDeviceAddress(); // Get current thruster address
void setDeviceAddress(uint8_t address); // Set thruster address (write to EEPROM)

uint8_t getDeviceType(); // Get this device type

void setRpm(uint16_t rpm); // Set new rpm value
void setPower(int8_t power); // Set new power value
void setCurrent(uint16_t current); // Set new current value

uint16_t getRpm(); // Get last rpm
int8_t getPower(); // Get last power
uint16_t getCurrent(); // Get last current

ThrusterStatus getThrusterStatus(); // Get last thruster info

void handleInputSpeed(int8_t speed); // Handler for input message

#endif