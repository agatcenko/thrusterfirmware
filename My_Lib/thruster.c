#include "thruster.h"

// Address of thruster
#define THRUSTER_ADDRESS 0x0A // Change if necessary before upload firmware 
#define ADC_SAMPLE_SIZE 5 // Number of adc samples before filtering
#define SPEED_BOUND 98 // bound for speed value 

uint16_t rotations = 0;
BitStatus rpmPin = 0, prevRpmPin = 0;

uint8_t sampleCounter = 0; // counter for adc samples
uint16_t realCurrent = 0;
uint16_t rawCurrentFiltered = 0;
uint16_t rawCurrent[ADC_SAMPLE_SIZE];

const uint16_t currentCoef = 2;
const uint16_t currentOffset = 6;

uint8_t thisDeviceType = 1; // 1 - Thruster
uint8_t thisDeviceAddress = 1; // Current thruster address
ThrusterStatus thrusterStatus;

uint16_t msgDeltaTime = 0; // Counter for time without control messages

// Passive wait function
static void delay(uint16_t nCount)
{
  while (nCount != 0) {
    nCount--;
  }
}

void initThruster()
{
  // Rpm count pins initialization
  GPIO_Init(FG1_PORT, FG1_PIN, GPIO_MODE_IN_PU_NO_IT);
  GPIO_Init(FG2_PORT, FG2_PIN, GPIO_MODE_IN_PU_NO_IT);
  
  // Thruster control pins initialization
  
  // Old values
  //GPIO_Init(BREAK_PORT, BREAK_PIN, GPIO_MODE_OUT_OD_LOW_FAST);
  //GPIO_Init(THRUSTER_DIR_PORT, THRUSTER_DIR_PIN, GPIO_MODE_OUT_OD_LOW_FAST);
  
  // Need to check
  GPIO_Init(BREAK_PORT, BREAK_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(THRUSTER_DIR_PORT, THRUSTER_DIR_PIN, GPIO_MODE_OUT_PP_LOW_FAST);

  TIM4_DeInit();
  TIM4_TimeBaseInit(TIM4_PRESCALER_128, 10);
  TIM4_Cmd(ENABLE);
  ITC_SetSoftwarePriority(ITC_IRQ_TIM4_OVF, ITC_PRIORITYLEVEL_3);
  TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE);
}

void readEepromValues()
{
  FLASH_Unlock(FLASH_MEMTYPE_DATA);
  thisDeviceAddress = FLASH_ReadByte(0x4000);
  
  // EEPROM have 0 value at 0x4000 address if it is first firmware upload
  if (thisDeviceAddress == 0x00) {
    setDeviceAddress(THRUSTER_ADDRESS);
  }
}

void setThrusterSpeed(int8_t speed)
{
  // Calculate rpm
  prevRpmPin = rpmPin;
  rpmPin = GPIO_ReadInputPin(FG1_PORT, FG1_PIN);
  if (!(prevRpmPin)&&(rpmPin)) {
    rotations++;
  }
  
  if (speed > 0) {
    TIM2_SetCompare2(speed);
    GPIO_WriteHigh(BREAK_PORT, BREAK_PIN); // No break
    GPIO_WriteHigh(THRUSTER_DIR_PORT, THRUSTER_DIR_PIN); // Forward direction
  }
  else if (speed < 0) {
    TIM2_SetCompare2(-speed);
    GPIO_WriteHigh(BREAK_PORT, BREAK_PIN); // No break
    GPIO_WriteLow(THRUSTER_DIR_PORT, THRUSTER_DIR_PIN); // Reverse direction
  }
  else if (speed == 0) {
    TIM2_SetCompare2(speed);
    GPIO_WriteLow(BREAK_PORT, BREAK_PIN); // Break
  }
}

void initAdc()
{
  // Initialize adc pins
  GPIO_Init(GPIOD, GPIO_PIN_2, GPIO_MODE_IN_FL_NO_IT);
  ADC1_Init(ADC1_CONVERSIONMODE_SINGLE, ADC1_CHANNEL_3, ADC1_PRESSEL_FCPU_D2, ADC1_EXTTRIG_TIM, DISABLE, ADC1_ALIGN_RIGHT, ADC1_SCHMITTTRIG_CHANNEL3, DISABLE);
  
  // Initialize timer for current measurement interrupt, 16*10^6/128/12500 = 0.1 s (10Hz)
  TIM1_TimeBaseInit(128, TIM1_COUNTERMODE_UP, 12500, 0);
  ITC_SetSoftwarePriority(ITC_IRQ_TIM1_OVF, ITC_PRIORITYLEVEL_2);
  TIM1_ITConfig(TIM1_IT_UPDATE, ENABLE);
  TIM1_Cmd(ENABLE);
}

void readCurrent()
{
  if (sampleCounter >= ADC_SAMPLE_SIZE) {
    sampleCounter = 0;
  }
  
  // Read raw alues from adc
  ADC1_ConversionConfig(ADC1_CONVERSIONMODE_SINGLE, ADC1_CHANNEL_3, ADC1_ALIGN_RIGHT);
  delay(5);
  ADC1_StartConversion();
  delay(5);
  rawCurrent[sampleCounter] = ADC1_GetConversionValue();
  sampleCounter++; // Increment adc samples counter
  
  if (sampleCounter == ADC_SAMPLE_SIZE) { // Collect necessary number of samples
    sampleCounter = 0;
    
    // Simple sort  
    for (uint8_t i = 0; i < ADC_SAMPLE_SIZE - 1; ++i) {
      for (uint8_t j = 0; j < ADC_SAMPLE_SIZE - 1; ++j) {
        if (rawCurrent[j] > rawCurrent[j + 1]) {
          uint16_t tmp = rawCurrent[j];
          rawCurrent[j] = rawCurrent[j + 1];
          rawCurrent[j + 1] = tmp;
        }
      }
    }
  }
  
  // Get median value
  rawCurrentFiltered = rawCurrent[ADC_SAMPLE_SIZE / 2];
  // Calculate current using offset and coefficient (hardcoded values)
  realCurrent = currentOffset + rawCurrentFiltered * currentCoef;
  setCurrent(realCurrent);
}

void calculateRpm()
{
  setRpm(rotations / 2);
  rotations = 0;
}

void clearMsgTimer()
{
  msgDeltaTime = 0;
}

void checkConnection()
{
  msgDeltaTime++;
  if (msgDeltaTime > 30) { // 3 seconds after last control msg
    msgDeltaTime = 0;
    if (thrusterStatus.power != 0) {
      setPower(0); // Power off thruster
    }
  }
}

uint8_t getDeviceAddress()
{
  return thisDeviceAddress;
}

void setDeviceAddress(uint8_t address)
{
  thisDeviceAddress = address;
  FLASH_ProgramByte(0x4000, thisDeviceAddress);
}

uint8_t getDeviceType()
{
  return thisDeviceType;
}

void setRpm(uint16_t rpm)
{
  thrusterStatus.rpm = rpm;
}
  
void setPower(int8_t power)
{
  thrusterStatus.power = power;
}

void setCurrent(uint16_t current)
{
  thrusterStatus.current = current;
}

uint16_t getRpm()
{
  return thrusterStatus.rpm;
}

int8_t getPower()
{
  return thrusterStatus.power;
}

uint16_t getCurrent()
{
  return thrusterStatus.current;
}

ThrusterStatus getThrusterStatus()
{
  return thrusterStatus;
}

void handleInputSpeed(int8_t speed)
{
  // Bound input speed and save it
  if (speed < -SPEED_BOUND) {
    speed = -SPEED_BOUND;
  }
  if (speed > SPEED_BOUND) {
    speed = SPEED_BOUND;
  }
  
  setPower(speed);
}