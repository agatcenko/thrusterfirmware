#ifndef UART_UTILS_H
#define UART_UTILS_H

void initConnection(); // Initialize uart1

void activateReceive(); // Activate receive mode for rs-485
void activateTransmit(); // Activate transmit mode for rs-485

void transmitData(uint8_t *data, uint8_t len); // Transmit buffer data

void handleReceivedByte(uint8_t byte); // Analyze next byte from rs-485 input
void processReceivedBuffer();

void handleBroadcastControl();

void prepareTransmitBuffer(uint8_t size);

void transmitErrorMsg();
void transmitTypeMsg();
void transmitAddressMsg();
void transmitStatusMsg();

uint16_t updateCRC(uint16_t acc, const uint8_t input);
uint16_t calculateCRC(const uint8_t *data, const int32_t len);

#endif